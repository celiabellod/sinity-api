.DEFAULT_GOAL := help
.PHONY: help
help:
	@echo "\033[33mUsage:\033[0m\n  make [target] [arg=\"val\"...]\n\n\033[33mTargets:\033[0m"
	@grep -E '^[a-zA-Z0-9_-]+:.*?## .*$$' Makefile| sort | awk 'BEGIN {FS = ":.*?## "}; {printf "  \033[32m%-15s\033[0m %s\n", $$1, $$2}'

.PHONY: bash
bash: ## Get a shell into app container
	docker-compose exec api bash

.PHONY: logs
logs: ## Get a shell into app container
	docker-compose logs -f api

.PHONY: install
install:  ## Install project
	docker-compose up --build -d
	docker-compose exec api alembic upgrade head
	docker-compose exec api python dbInit.py
	docker-compose exec api python -m appli.fixtures.main

.PHONY: up
up: ## Start containers
	docker-compose up -d
	docker-compose exec api pip freeze > ./api/requirements.txt

.PHONY: stop
stop: ## Stop containers
	-docker-compose stop

.PHONY: restart
restart: ## Restart containers
	docker-compose restart

.PHONY: db-dump
db-dump: ## backup db 
	docker-compose exec postgres /usr/bin/pg_dump -U root project > ./api/sql-database/backup.sql

.PHONY: db-restore
db-restore: ## Restore db
	docker-compose exec -T postgres /bin/bash -c "PGPASSWORD=root psql --username root project" < project/api/sql-database/backup.sql

.PHONY: db-init
db-init:  ## Install project
	docker-compose exec api python dbInit.py

.PHONY: hash-password
hash-password:  ## Install project
	docker-compose exec api python hashPasswordCommande.py

.PHONY: fixtures
fixtures:  ## make fixtures
	docker-compose exec api python -m appli.fixtures.main

.PHONY: dependances
dependances:  ## make fixtures
	docker-compose exec api pip freeze > ./api/requirements.txt

.PHONY: migrations
migrations:  ## make migrations
	docker-compose exec api alembic revision --autogenerate

.PHONY: migrate
migrate:  ## push migrations
	docker-compose exec api alembic upgrade head

.PHONY: cc
cc:  ## clear cache
	find ./api | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs sudo rm -rf

.PHONY: tests
tests:  ## Install project
	docker-compose exec api pytest -s -v