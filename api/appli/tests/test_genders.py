# # pylint: disable=missing-module-docstring
# from fastapi.testclient import TestClient
# from pytest_schema import schema
# from ..main import app
# from ._schema import gender
# from ._get_token import _get_token
# from ._fixtures import *

# client = TestClient(app)

# @pytest.mark.usefixtures("fixture_create_admin")
# def test_get_genders():
#     """ Test for get all genders """

#     json_genders = [gender]

#     token = _get_token(client)

#     response = client.get("/genders", headers={"Authorization": token.get(
#         'token_type').capitalize() + ' ' + token.get('access_token')})
    
#     assert response.status_code == 200
#     assert schema(json_genders) == response.json()


# @pytest.mark.usefixtures("fixture_create_admin")
# def test_get_gender():
#     """ Test for get a gender """

#     token = _get_token(client)

#     response = client.get("/genders/1", headers={"Authorization": token.get(
#         'token_type').capitalize() + ' ' + token.get('access_token')})
    
#     assert response.status_code == 200
#     assert response.json() == {
#         "id": 1,
#         "name": "Male",
#     }


# def test_get_genders_bad_token():
#     """ Test for get all gender with bad token """

#     response = client.get("/genders", headers={"Authorization": "falseToken"})
    
#     assert response.status_code == 401
#     assert response.json() == {"detail": "Not authenticated"}


# @pytest.mark.usefixtures("fixture_create_admin")
# def test_get_inexistent_gender():
#     """ Test for get an inexistent gender """

#     token = _get_token(client)

#     response = client.get("/genders/0",  headers={"Authorization": token.get(
#         'token_type').capitalize() + ' ' + token.get('access_token')})
    
#     assert response.status_code == 404
#     assert response.json() == {"detail": "Gender not found"}
