from fastapi.testclient import TestClient
from pytest_schema import schema
from ..main import app
from ._get_token import _get_token
import pytest
from faker import Faker
from sqlalchemy.orm import Session
from fastapi import Depends
from passlib.context import CryptContext
from ..models import User, AstroSign, Gender, City, Role, Match
from ..database import SessionLocal
from ..crud import user as user_crud
from ..crud import match as match_crud
from ..schemas import Admin
from ..crud import admin as crud_admin

faker = Faker()

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

client = TestClient(app)

token = _get_token(client)

@pytest.fixture
def fixture_create_user():
    user = user_crud.create_user(db=SessionLocal(), user=_create_user())
    yield user
    user_crud.delete_user(db=SessionLocal(), user_id=user.id)
    
@pytest.fixture
def fixture_create_admin():
    admin = Admin.AdminIn(
        firstname="Célia",
        lastname="Bellod",
        email="celia@test.fr",
        password=pwd_context.hash("admin"),
    )
    admin = crud_admin.create_admin(db=SessionLocal(), admin=admin)
    yield admin
    crud_admin.delete_admin(db=SessionLocal(), admin_id=admin.id)

@pytest.fixture
def fixture_create_match():
    user = user_crud.create_user(db=SessionLocal(), user=_create_user())
    match_user = user_crud.create_user(db=SessionLocal(), user=_create_user())
    
    match = Match.Match(
        user_id = user.id,
        match_user_id = match_user.id
    )
    match = match_crud.create_match(db=SessionLocal(), match=match)
    yield match
    match_crud.delete_match(db=SessionLocal(), match_id=match.id)
    user_crud.delete_user(db=SessionLocal(), user_id=user.id)
    user_crud.delete_user(db=SessionLocal(), user_id=match_user.id)
    
def _create_user():
    user = User.User(
        firstname=faker.name(),
        email=faker.email(),
        password=pwd_context.hash(faker.pystr()),
        birthdate=faker.date(),
        astro_sign_id=faker.pyint(min_value=1, max_value=12, step=1),
        city_id=faker.pyint(min_value=1, max_value=249, step=1),
        gender_id=faker.pyint(min_value=1, max_value=2, step=1),
        search_astro_sign_id=faker.pyint(
            min_value=1, max_value=12, step=1),
        search_gender_id=faker.pyint(min_value=1, max_value=2, step=1),
        search_city_id=faker.pyint(min_value=1, max_value=249, step=1),
        verif_account=False,
        verif_email=False,
        role_id=1
    )
    return user