# pylint: disable=missing-module-docstring
from fastapi.testclient import TestClient
from pytest_schema import schema
from ..main import app
from ._schema import role
from ._get_token import _get_token
from ._fixtures import *

client = TestClient(app)

@pytest.mark.usefixtures("fixture_create_admin")
def test_get_roles():
    """ Test for get all roles """

    json_roles = [role]

    token = _get_token(client)

    response = client.get("/roles", headers={"Authorization": token.get(
        'token_type').capitalize() + ' ' + token.get('access_token')})
    assert response.status_code == 200
    assert schema(json_roles) == response.json()

@pytest.mark.usefixtures("fixture_create_admin")
def test_get_role():
    """ Test for get a role """

    token = _get_token(client)

    response = client.get("/roles/1", headers={"Authorization": token.get(
        'token_type').capitalize() + ' ' + token.get('access_token')})
    assert response.status_code == 200
    assert response.json() == {
        "id": 1,
        "name": "USER",
    }


def test_get_roles_bad_token():
    """ Test for get all role with bad token """

    response = client.get("/roles", headers={"Authorization": "falseToken"})
    assert response.status_code == 401
    assert response.json() == {"detail": "Not authenticated"}


@pytest.mark.usefixtures("fixture_create_admin")
def test_get_inexistent_role():
    """ Test for get an inexistent role """

    token = _get_token(client)

    response = client.get("/roles/0",  headers={"Authorization": token.get(
        'token_type').capitalize() + ' ' + token.get('access_token')})
    assert response.status_code == 404
    assert response.json() == {"detail": "role not found"}
