role = {
    "id": int,
    "name": str
}

astro_sign = {
    "id": int,
    "name": str,
    "dateFrom": str,
    "dateTo": str
}

city = {
    "id": int,
    "department_code": int,
    "inseeCode": str,
    "zipCode": str,
    "name": str,
    "slug": str,
    "gpsLat": float,
    "gpsLng": float,
}

gender = {
    "id": int,
    "name": str
}

region = {
    "id": int,
    "code": str,
    "name": str,
    "slug": str,
}

department = {
    "id": int,
    "region_code": int,
    "code": str,
    "name": str,
    "slug": str,
}
  
user_in = {
    "id": int,
    "firstname": str,
    "email": str,
    "birthdate": str,
    "password": str,
    "astro_sign_id": int,
    "city_id": int,
    "gender_id": int,
    "search_astro_sign_id": int,
    "search_gender_id": int,
    "search_city_id": int,
    "verif_email": bool,
    "verif_account": bool,
    "role_id": int
}

user_out = {
    "id": int,
    "firstname": str,
    "email": str,
    "birthdate": str,
    "astro_sign": astro_sign,
    "city": city,
    "gender": gender,
    "search_astro_sign": astro_sign,
    "search_gender": gender,
    "search_city": city,
    "verif_email": bool,
    "verif_account": bool,
    "role": role
}

match_in = {
    "user_id": int,
    "match_user_id": int
}

match_out = {
    "id": int,
    "user": user_out,
    "match_user": user_out
}
