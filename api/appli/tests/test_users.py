from fastapi.testclient import TestClient
from pytest_schema import schema
from typing import Optional
from datetime import date
from ._schema import user_in, user_out
from ..main import app
from ._get_token import _get_token
from ._fixtures import *

client = TestClient(app)

@pytest.mark.usefixtures("fixture_create_user", "fixture_create_admin")
def test_get_users():
    """ Test for get all user """
    users = [user_out]
    
    token = _get_token(client)
    
    response = client.get("/users", headers={"Authorization": token.get(
        'token_type').capitalize() + ' ' + token.get('access_token')})

    assert response.status_code == 200
    assert schema(users) == response.json()


def test_get_users_bad_token():
    """ Test for get all user with bad token """

    response = client.get("/users", headers={"Authorization": "falseToken"})
    assert response.status_code == 401
    assert response.json() == {"detail": "Not authenticated"}


@pytest.mark.usefixtures("fixture_create_admin")
def test_get_inexistent_user():
    """ Test for get an inexistent user """

    token = _get_token(client)
    
    response = client.get("/users/0",  headers={"Authorization": token.get(
        'token_type').capitalize() + ' ' + token.get('access_token')})
    assert response.status_code == 404
    assert response.json() == {"detail": "User not found"}


@pytest.mark.usefixtures("fixture_create_admin")
def test_create_user():
        
    response = client.post(
        "/users",
        json={
            "firstname": "Célia",
            "email": "celia@test.fr",
            "password": "test",
            "birthdate": "1997-05-25",
            "astro_sign_id": 1,
            "city_id": 2,
            "gender_id": 2,
            "search_astro_sign_id": 1,
            "search_gender_id": 1,
            "search_city_id": 2,
            "role_id": 2
        }
    )

    assert response.status_code == 200
    assert schema(user_in) == response.json()

    client.delete(
        "/users/" + str(response.json().get('id')),
        headers={"Authorization": token.get(
            'token_type').capitalize() + ' ' + token.get('access_token')},
    )


@pytest.mark.usefixtures("fixture_create_user", "fixture_create_admin")
def test_delete_user(fixture_create_user):

    token = _get_token(client)

    response = client.delete(
        "/users/" + str(fixture_create_user.id),
        headers={"Authorization": token.get(
            'token_type').capitalize() + ' ' + token.get('access_token')},
    )

    assert response.status_code == 200
    assert response.json() == "User delete with success."


@pytest.mark.usefixtures("fixture_create_user","fixture_create_admin")
def test_get_user(fixture_create_user):
    """ Test for get a user"""
    
    token = _get_token(client)

    response = client.get("/users/" + str(fixture_create_user.id),
                          headers={"Authorization": token.get('token_type').capitalize() + ' ' + token.get('access_token')})

    assert response.status_code == 200
    assert schema(user_out) == response.json()
    
    
@pytest.mark.usefixtures("fixture_create_user", "fixture_create_admin")
def test_update_user(fixture_create_user):

    token = _get_token(client)
    
    response = client.put(
        "/users/" + str(fixture_create_user.id),
        headers={"Authorization": token.get(
            'token_type').capitalize() + ' ' + token.get('access_token')},
        json={
            "firstname": "Célia",
            "email": "celia@user_test.fr",
            "password": "test",
            "birthdate": "1997-05-25",
            "astro_sign_id": 1,
            "city_id": 2,
            "gender_id": 2,
            "search_astro_sign_id": 1,
            "search_gender_id": 1,
            "search_city_id": 2,
            "role_id": 2
        }
    )

    assert response.status_code == 200
    assert schema(user_out) == response.json()

    client.delete(
        "/users/" + str(response.json().get('id')),
        headers={"Authorization": token.get(
            'token_type').capitalize() + ' ' + token.get('access_token')},
    )

@pytest.mark.usefixtures("fixture_create_user", "fixture_create_admin")
def test_create_existing_user(fixture_create_user):
    
    token = _get_token(client)
    
    response = client.post(
        "/users",
        headers={"Authorization": token.get(
            'token_type').capitalize() + ' ' + token.get('access_token')},
        json={
            "firstname": fixture_create_user.firstname,
            "email": fixture_create_user.email,
            "password": fixture_create_user.password,
            "birthdate": str(fixture_create_user.birthdate),
            "astro_sign_id": fixture_create_user.astro_sign_id,
            "city_id": fixture_create_user.city_id,
            "gender_id": fixture_create_user.gender_id,
            "search_astro_sign_id": fixture_create_user.search_astro_sign_id,
            "search_gender_id": fixture_create_user.search_gender_id,
            "search_city_id": fixture_create_user.search_city_id,
            "role_id": fixture_create_user.role_id
        }
    )

    assert response.status_code == 400
    assert response.json() == {"detail": "User already registered"}

