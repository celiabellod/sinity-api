# pylint: disable=missing-module-docstring
from fastapi.testclient import TestClient
from pytest_schema import schema
from ..main import app
from ._schema import department
from ._get_token import _get_token
from ._fixtures import *

client = TestClient(app)

@pytest.mark.usefixtures("fixture_create_admin")
def test_get_departments():
    """ Test for get all department """

    departments = [department]

    token = _get_token(client)

    response = client.get("/departments", headers={"Authorization": token.get(
        'token_type').capitalize() + ' ' + token.get('access_token')})
    
    assert response.status_code == 200
    assert schema(departments) == response.json()


@pytest.mark.usefixtures("fixture_create_admin")
def test_get_department():
    """ Test for get a department"""

    token = _get_token(client)

    response = client.get("/departments/1", headers={"Authorization": token.get(
        'token_type').capitalize() + ' ' + token.get('access_token')})
    
    assert response.status_code == 200
    assert response.json() == {
        "id": 1,
        "region_code": 84,
        "code": "01",
        "name": "Ain",
        "slug": "ain",
    }


def test_get_departments_bad_token():
    """ Test for get all department with bad token """

    response = client.get("/departments", headers={"Authorization": "falseToken"})
    
    assert response.status_code == 401
    assert response.json() == {"detail": "Not authenticated"}


@pytest.mark.usefixtures("fixture_create_admin")
def test_get_inexistent_department():
    """ Test for get an inexistent department """

    token = _get_token(client)

    response = client.get("/departments/0",  headers={"Authorization": token.get(
        'token_type').capitalize() + ' ' + token.get('access_token')})
    
    assert response.status_code == 404
    assert response.json() == {"detail": "Department not found"}
