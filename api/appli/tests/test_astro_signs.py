# pylint: disable=missing-module-docstring
from fastapi.testclient import TestClient
from pytest_schema import schema
from ..main import app
from ._schema import astro_sign
from ._get_token import _get_token
from ._fixtures import *

client = TestClient(app)

@pytest.mark.usefixtures("fixture_create_admin")
def test_get_astro_signs():
    """ Test for get all astro sign """

    astro_signs = [astro_sign]
    
    response = client.get("/astroSigns")
    
    assert response.status_code == 200
    assert schema(astro_signs) == response.json()


@pytest.mark.usefixtures("fixture_create_admin")
def test_get_astro_sign():
    """ Test for get a astro sign"""

    token = _get_token(client)

    response = client.get("/astroSigns/1", headers={"Authorization": token.get(
        'token_type').capitalize() + ' ' + token.get('access_token')})
    
    assert response.status_code == 200
    assert response.json() == {
        "id": 1,
        "name": "Gemini",
        "dateFrom": "1970-05-21",
        "dateTo": "1970-06-21"
    }


# def test_get_astro_signs_bad_token():
#     """ Test for get all astro sign with bad token """

#     response = client.get("/astroSigns/1", headers={"Authorization": "falseToken"})
    
#     assert response.status_code == 401
#     assert response.json() == {"detail": "Not authenticated"}


@pytest.mark.usefixtures("fixture_create_admin")
def test_get_inexistent_astro_sign():
    """ Test for get an inexistent astro sign """

    token = _get_token(client)

    response = client.get("/astroSigns/0",  headers={"Authorization": token.get(
        'token_type').capitalize() + ' ' + token.get('access_token')})
    
    assert response.status_code == 404
    assert response.json() == {"detail": "Astro sign not found"}
