# pylint: disable=missing-module-docstring
from fastapi.testclient import TestClient
from pytest_schema import schema
from ..main import app
from ._schema import match_in, match_out
from ._get_token import _get_token
from ._fixtures import *
from ._fixtures import *

client = TestClient(app)

@pytest.mark.usefixtures("fixture_create_match", "fixture_create_admin")
def test_get_matches():
    """ Test for get all matches """

    json_matches = [match_out]

    token = _get_token(client)

    response = client.get("/matches", headers={"Authorization": token.get(
        'token_type').capitalize() + ' ' + token.get('access_token')})

    assert response.status_code == 200
    assert schema(json_matches) == response.json()


@pytest.mark.usefixtures("fixture_create_match", "fixture_create_admin")
def test_get_match(fixture_create_match):
    """ Test for get a match """

    token = _get_token(client)
    response = client.get("/matches/" + str(fixture_create_match.id), headers={"Authorization": token.get(
        'token_type').capitalize() + ' ' + token.get('access_token')})
    
    assert response.status_code == 200
    assert schema(match_out) == response.json()


@pytest.mark.usefixtures("fixture_create_match")
def test_get_matches_bad_token():
    """ Test for get all match with bad token """

    response = client.get("/matches", headers={"Authorization": "falseToken"})
    assert response.status_code == 401
    assert response.json() == {"detail": "Not authenticated"}


@pytest.mark.usefixtures("fixture_create_match", "fixture_create_admin")
def test_get_inexistent_match(fixture_create_match):
    """ Test for get an inexistent match """

    token = _get_token(client)

    response = client.get("/matches/" + str(fixture_create_match.user_id) + "/0",  headers={"Authorization": token.get(
        'token_type').capitalize() + ' ' + token.get('access_token')})
    assert response.status_code == 404
    assert response.json() == {"detail": "Match not found"}
