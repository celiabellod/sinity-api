# pylint: disable=missing-module-docstring
from fastapi.testclient import TestClient
from pytest_schema import schema
from ..main import app
from ._schema import region
from ._get_token import _get_token
from ._fixtures import *

client = TestClient(app)

@pytest.mark.usefixtures("fixture_create_admin")
def test_get_regions():
    """ Test for get all region """

    regions = [region]

    token = _get_token(client)

    response = client.get("/regions", headers={"Authorization": token.get(
        'token_type').capitalize() + ' ' + token.get('access_token')})
    
    assert response.status_code == 200
    assert schema(regions) == response.json()

@pytest.mark.usefixtures("fixture_create_admin")
def test_get_region():
    """ Test for get a region"""

    token = _get_token(client)

    response = client.get("/regions/1", headers={"Authorization": token.get(
        'token_type').capitalize() + ' ' + token.get('access_token')})
    
    assert response.status_code == 200
    assert response.json() == {
        "id": 1,
        "code": '01',
        "name": "Guadeloupe",
        "slug": "guadeloupe",
    }


def test_get_regions_bad_token():
    """ Test for get all region with bad token """

    response = client.get("/regions", headers={"Authorization": "falseToken"})
    
    assert response.status_code == 401
    assert response.json() == {"detail": "Not authenticated"}

@pytest.mark.usefixtures("fixture_create_admin")
def test_get_inexistent_region():
    """ Test for get an inexistent region """

    token = _get_token(client)

    response = client.get("/regions/0",  headers={"Authorization": token.get(
        'token_type').capitalize() + ' ' + token.get('access_token')})
    
    assert response.status_code == 404
    assert response.json() == {"detail": "Region not found"}
