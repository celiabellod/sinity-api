# pylint: disable=missing-module-docstring
from fastapi.testclient import TestClient
from pytest_schema import schema
from ..main import app
from ._schema import city
from ._get_token import _get_token
from ._fixtures import *

client = TestClient(app)

@pytest.mark.usefixtures("fixture_create_admin")
def test_get_cities():
    """ Test for get all city """

    cities = [city]

    response = client.get("/cities")
    
    assert response.status_code == 200
    assert schema(cities) == response.json()


@pytest.mark.usefixtures("fixture_create_admin")
def test_get_city():
    """ Test for get a city"""

    token = _get_token(client)

    response = client.get("/cities/1", headers={"Authorization": token.get(
        'token_type').capitalize() + ' ' + token.get('access_token')})
    
    assert response.status_code == 200
    assert response.json() == {
        "id": 1,
        "department_code": 1,
        "inseeCode": '01001',
        "zipCode": '01400',
        "name": "L'Abergement-Clémenciat",
        "slug": 'l abergement clemenciat',
        "gpsLat": 46.1568,
        "gpsLng": 4.9247,
    }


def test_get_city_bad_token():
    """ Test for get all city with bad token """

    response = client.get("/cities/1", headers={"Authorization": "falseToken"})
    
    assert response.status_code == 401
    assert response.json() == {"detail": "Not authenticated"}


@pytest.mark.usefixtures("fixture_create_admin")
def test_get_inexistent_city():
    """ Test for get an inexistent city """

    token = _get_token(client)

    response = client.get("/cities/0",  headers={"Authorization": token.get(
        'token_type').capitalize() + ' ' + token.get('access_token')})
    
    assert response.status_code == 404
    assert response.json() == {"detail": "City not found"}
