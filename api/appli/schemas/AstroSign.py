from pydantic import BaseModel
from datetime import date

class AstroSign(BaseModel):
    id: int
    name: str
    dateFrom: date
    dateTo: date
    
    class Config:
        orm_mode = True
