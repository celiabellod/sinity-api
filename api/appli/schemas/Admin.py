from typing import Optional
from pydantic import BaseModel

class AdminOut(BaseModel):
    id: int
    firstname: str
    lastname: str
    email: str

    class Config:
        orm_mode = True       
        
class AdminIn(BaseModel):
    id: Optional[int]
    firstname: str
    lastname: str
    email: str    
    password: str

    class Config:
      orm_mode = True       
  