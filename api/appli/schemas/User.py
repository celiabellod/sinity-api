from typing import List, Optional

from . import City, Gender, AstroSign, Role
from pydantic import BaseModel
from datetime import date
# from . import AstroSign, City, Gender

class UserOut(BaseModel):
    id: int
    firstname: str
    email: str
    birthdate: date
    astro_sign: AstroSign.AstroSign
    city: City.City
    gender: Gender.Gender
    fb: Optional[str]
    instagram: Optional[str]
    snapchat: Optional[str]
    biography: Optional[str]
    search_astro_sign: AstroSign.AstroSign
    search_gender: Gender.Gender
    search_city: City.City
    verif_email: Optional[bool]
    verif_account: Optional[bool]
    role: Role.Role
    #matches: List[Match.MatchOut] = []

    class Config:
        orm_mode = True       
        
class UserIn(BaseModel):
    id: Optional[int]
    firstname: str
    email: str    
    password: str
    birthdate: date
    astro_sign_id: int
    city_id: int
    gender_id: int
    fb: Optional[str]
    instagram: Optional[str]
    snapchat: Optional[str]
    biography: Optional[str]
    search_astro_sign_id: int
    search_gender_id: int
    search_city_id: int
    verif_email: Optional[bool]
    verif_account: Optional[bool]
    role_id:  Optional[int]
    
    class Config:
      orm_mode = True       
  
#update_data = user.dict(exclude_unset=True, exclude_none=True)

class UserToken(BaseModel):
    id: int
    firstname: str
    lastname: Optional[str]
    email: str
    birthdate: Optional[date]
    astro_sign: Optional[AstroSign.AstroSign]
    city: Optional[City.City]
    gender: Optional[Gender.Gender]
    fb: Optional[str]
    instagram: Optional[str]
    snapchat: Optional[str]
    biography: Optional[str]
    search_astro_sign: Optional[AstroSign.AstroSign]
    search_gender: Optional[Gender.Gender]
    search_city: Optional[City.City]
    verif_email: Optional[bool]
    verif_account: Optional[bool]
    role: Optional[Role.Role]
    
    class Config:
      orm_mode = True