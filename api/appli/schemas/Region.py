from pydantic import BaseModel

class Region(BaseModel):
    id: int
    code: str
    name: str
    slug: str
    
    class Config:
        orm_mode = True