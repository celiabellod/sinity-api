from pydantic import BaseModel
from . import Region

class Department(BaseModel):
    id: int
    region_code: int
    code: str
    name: str
    slug: str
    
    class Config:
        orm_mode = True