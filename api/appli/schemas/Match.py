from pydantic import BaseModel
from . import User
from typing import List, Optional

class MatchOut(BaseModel):
    id: int
    user: User.UserOut
    match_user: User.UserOut
    
    class Config:
        orm_mode = True

class MatchIn(BaseModel):
    id: Optional[int]
    user_id: int
    match_user_id: int
    
    class Config:
        orm_mode = True