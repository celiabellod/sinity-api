from pydantic import BaseModel
from typing import Optional
from . import City, Gender, AstroSign, Role
from pydantic import BaseModel
from datetime import date
from typing import Optional
from . import User

class TokenData(BaseModel):
    username: Optional[str] = None
class Token(BaseModel):
    user_data: User.UserToken
    access_token: str
    token_type: str
    refresh_token: str