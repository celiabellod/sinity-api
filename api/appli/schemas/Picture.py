from pydantic import BaseModel
from typing import List, Optional

class Picture(BaseModel):
  id: int
  name: str
  user_id: int
  
  class Config:
    orm_mode=True