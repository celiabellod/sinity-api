from pydantic import BaseModel
from . import Department

class City(BaseModel):
    id: int
    department_code: int
    inseeCode: str
    zipCode: str
    name: str
    slug: str
    gpsLat: float
    gpsLng: float
    
    class Config:
        orm_mode = True