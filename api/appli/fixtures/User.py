from faker import Faker
from sqlalchemy.orm import Session
from fastapi import Depends
from passlib.context import CryptContext
from ..models import User, AstroSign, Gender, City, Role
from ..database import SessionLocal
from ..crud import user as crud
from datetime import date
import random

faker = Faker()

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

def userFixtures():                                     
    for i in range(10):
        user = User.User(
            firstname = faker.name(),
            email = faker.email(), 
            password = pwd_context.hash(faker.pystr()),
            birthdate = faker.date_between_dates(date_start=date.fromisoformat('1921-12-31'), date_end=date.fromisoformat('2003-12-31')),
            astro_sign_id = faker.pyint(min_value=1, max_value=12, step=1) ,
            city_id = faker.pyint(min_value=1, max_value=249, step=1),
            gender_id = faker.pyint(min_value=1, max_value=2, step=1),
            search_astro_sign_id = faker.pyint(min_value=1, max_value=12, step=1),
            search_gender_id =  faker.pyint(min_value=1, max_value=2, step=1),
            search_city_id = faker.pyint(min_value=1, max_value=249, step=1),
            verif_account = False,
            verif_email = False,
            role_id = 1
        )   
        crud.create_user(db=SessionLocal(), user=user)
        