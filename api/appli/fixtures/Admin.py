from passlib.context import CryptContext
from ..database import SessionLocal
from ..crud import admin as crud
from ..schemas import Admin
from ..crud import admin as crud
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

def adminFixtures(): 
    admin = Admin.AdminIn(
        firstname = "Célia",
        lastname = "Bellod",
        email = "celia@sinity.fr", 
        password = pwd_context.hash("admin"),
    )
    crud.create_admin(db=SessionLocal(), admin=admin)