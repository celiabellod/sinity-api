from fastapi import FastAPI
from .routers import match,astro_sign,city,gender,department,region,user,token,picture,role
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI(
    title="Astro API",
    description="This is meet people project, with auto docs for the API",
    version="1.0.0",
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(user.router)
app.include_router(match.router)
app.include_router(gender.router)
app.include_router(astro_sign.router)
app.include_router(city.router)
app.include_router(department.router)
app.include_router(region.router)
app.include_router(token.router)
app.include_router(picture.router)
app.include_router(role.router)
