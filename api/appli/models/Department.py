from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from ..database import Base

class Department(Base):
    __tablename__ = 'departments'


    id = Column(
        Integer, 
        primary_key=True
    )
    
    region_code =  Column(
        String(3), 
        ForeignKey('regions.code', onupdate="CASCADE", ondelete="CASCADE"),
        nullable=False
    )
    region = relationship('Region')

    
    code = Column(
        String(3), 
        unique=True, 
        nullable=False
    )
    
    name = Column(
        String(255), 
        nullable=False
    )
    
    slug = Column(
        String(255), 
        nullable=False
    )

    def __init__(self, regionCode, code, name, slug):
        self.regionCode = regionCode
        self.code = code
        self.name = name
        self.slug = slug

    def __repr__(self): 
        return '<Département {}>'.format(self.name)