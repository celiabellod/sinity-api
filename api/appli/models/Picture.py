from sqlalchemy import Boolean, Column, ForeignKey, Integer, String,  Text
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import Date

from ..database import Base


class Picture(Base):
    __tablename__ = 'pictures'

    id = Column(
        Integer(),
        primary_key=True
    )
    name = Column(
        String(255),
        nullable=False
    )
    user_id = Column(
        Integer, 
        ForeignKey('users.id'), 
        name="user_id",
        nullable=False
    )
    user = relationship('User')

    def __init__(self, name, user_id):
        self.name = name
        self.user_id = user_id

    def __repr__(self):
        return '<Picture {}>'.format(self.image)
