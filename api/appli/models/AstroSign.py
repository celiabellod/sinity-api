from sqlalchemy import Column, Integer, String, Date
from sqlalchemy.orm import relationship

from ..database import Base

class AstroSign(Base):
    __tablename__ = 'astro_signs'

    id = Column(
        Integer,
        primary_key=True
    )

    name = Column(
        String(100),
        nullable=False
    )

    dateFrom = Column(
        Date,
        name="date_from",
        nullable=False
    )

    dateTo = Column(
        Date,
        name="date_to",
        nullable=False
    )

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Signe astrologique {}>'.format(self.name)
