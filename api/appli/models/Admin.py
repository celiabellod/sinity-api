from sqlalchemy import Boolean, Column, ForeignKey, Integer, String,  Text
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import Date

from ..database import Base

class Admin(Base):
    __tablename__ = 'admin'

    id = Column(
        Integer(),
        primary_key=True
    )

    firstname = Column(
        String(),
        nullable=False
    )
    
    lastname = Column(
        String(),
        nullable=False
    )

    email = Column(
        String(50),
        nullable=False
    )

    password = Column(
        String(255),
        nullable=False
    )

    def __init__(self,
                 firstname,
                 lastname,
                 email,
                 password,
                ):

        self.firstname = firstname
        self.lastname = lastname
        self.email = email
        self.password = password
    
    def __repr__(self):
        return '<Administrateur {}>'.format(self.firstname)
