from sqlalchemy import Column, Integer, ForeignKey
from sqlalchemy.orm import relationship

from ..database import Base

class Match(Base):
    __tablename__ = 'matches'

    id = Column(
        Integer, 
        primary_key=True
    )
    
    user_id =  Column(
            Integer, 
            ForeignKey('users.id', onupdate="CASCADE", ondelete="CASCADE"),
            name="user_id",
            nullable=False,
        )

    user = relationship(
        'User', primaryjoin="User.id == Match.user_id")

    match_user_id = Column(
            Integer, 
            ForeignKey('users.id', onupdate="CASCADE", ondelete="CASCADE"),
            name="match_user_id",
            nullable=False,
        )
    
    match_user = relationship(
        'User', primaryjoin="User.id == Match.match_user_id")
    
    def __init__(self, user_id, match_user_id):
        self.user_id = user_id
        self.match_user_id = match_user_id

    def __repr__(self): 
        return '<Match {}>'.format(self.id)