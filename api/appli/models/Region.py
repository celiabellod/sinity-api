from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from ..database import Base

class Region(Base):
    __tablename__ = 'regions'

    id = Column(
        Integer(), 
        primary_key=True
    )
    code = Column(
        String(3), 
        unique=True, 
        nullable=False
    )
    name = Column(
        String(255), 
        nullable=False
    )
    slug = Column(
        String(255), 
        nullable=False
    )

    def __init__(self, code, name, slug):
        self.code = code
        self.name = name
        self.slug = slug

    def __repr__(self):
        return '<Région {}>'.format(self.name)