from sqlalchemy import Column, ForeignKey, Integer, String, Float
from sqlalchemy.orm import relationship

from ..database import Base

class City(Base):
    __tablename__ = 'cities'

    id = Column(
        Integer(), 
        primary_key=True
    )
    
    department_code = Column(
        String(), 
        ForeignKey('departments.code', onupdate="CASCADE", ondelete="CASCADE"),
        nullable=False
    )
    # department = relationship("Department")

    inseeCode = Column(
        String(5), 
        name="insee_code",
        nullable=True
    )
    
    zipCode = Column(
        String(5), 
        name="zip_code",
        nullable=True
    )
    
    name = Column(
        String(255), 
        nullable=False
    )
    
    slug = Column(
        String(255), 
        nullable=False
    )
    
    gpsLat = Column(
        Float(16,14),
        name="gps_lat", 
        nullable=False
    )
    
    gpsLng = Column(
        Float(17,14), 
        name="gps_lng",
        nullable=False
    )
    

    def __init__(self, departmentCode, inseeCode, zipCode,name, gpsLat, gpsLng, slug):
        self.departmentCode = departmentCode
        self.inseeCode = inseeCode
        self.zipCode = zipCode
        self.name = name
        self.gpsLat = gpsLat
        self.gpsLng = gpsLng
        self.slug = slug
            
    def __repr__(self):
        return '<Ville {}>'.format(self.name)

