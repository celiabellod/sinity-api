from sqlalchemy import Boolean, Column, ForeignKey, Integer, String,  Text
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import Date

from ..database import Base


class User(Base):
    __tablename__ = 'users'

    id = Column(
        Integer(),
        primary_key=True
    )

    firstname = Column(
        String(),
        nullable=False
    )

    email = Column(
        String(50),
        nullable=False
    )

    password = Column(
        String(255),
        nullable=False
    )

    birthdate = Column(
        Date(),
        nullable=False
    )

    astro_sign_id = Column(
        Integer(),
        ForeignKey('astro_signs.id', onupdate="CASCADE", ondelete="CASCADE"),
        name="astro_sign_id",
        nullable=False
    )
    astro_sign = relationship(
        "AstroSign", primaryjoin="AstroSign.id == User.astro_sign_id", viewonly=True)

    city_id = Column(
        Integer(),
        ForeignKey('cities.id', onupdate="CASCADE", ondelete="CASCADE"),
        name="city_id",
        nullable=False
    )
    city = relationship(
        'City', primaryjoin="City.id == User.city_id", viewonly=True)

    gender_id = Column(
        Integer(),
        ForeignKey('genders.id', onupdate="CASCADE", ondelete="CASCADE"),
        name="gender_id",
        nullable=False
    )
    gender = relationship(
        'Gender', primaryjoin="Gender.id == User.gender_id", viewonly=True)

    fb = Column(
        String(255),
        nullable=True,
    )
    instagram = Column(
        String(255),
        nullable=True,
    )
    snapchat = Column(
        String(255),
        nullable=True,
    )
    biography = Column(
        Text(),
        nullable=True,
    )
    search_astro_sign_id = Column(
        Integer(),
        ForeignKey('astro_signs.id', onupdate="CASCADE", ondelete="CASCADE"),
        name="search_astro_sign_id",
        nullable=False
    )
    search_astro_sign = relationship(
        'AstroSign', primaryjoin="AstroSign.id == User.astro_sign_id", viewonly=True)

    search_gender_id = Column(
        Integer(),
        ForeignKey('genders.id', onupdate="CASCADE", ondelete="CASCADE"),
        name="search_gender_id",
        nullable=False
    )
    search_gender = relationship(
        'Gender', primaryjoin="Gender.id == User.gender_id", viewonly=True)

    search_city_id = Column(
        Integer(),
        ForeignKey('cities.id', onupdate="CASCADE", ondelete="CASCADE"),
        name="search_city_id",
        nullable=False
    )
    search_city = relationship(
        'City', primaryjoin="City.id == User.city_id", viewonly=True)

    verif_email = Column(
        Boolean(),
        name="verif_email",
        nullable=False,
        default=False
    )

    verif_account = Column(
        Boolean(),
        name="verif_account",
        nullable=False,
        default=False
    )

    role_id = Column(
        Integer(),
        ForeignKey('roles.id', onupdate="CASCADE", ondelete="CASCADE"),
        name="role_id",
        nullable=False
    )
    role = relationship(
        'Role', primaryjoin="Role.id == User.role_id", viewonly=True)

    def __init__(self,
                 firstname,
                 email,
                 password,
                 birthdate,
                 astro_sign_id,
                 city_id,
                 gender_id,
                 search_astro_sign_id,
                 search_gender_id,
                 search_city_id,
                 verif_email,
                 verif_account,
                 role_id,
                 fb=None,
                 instagram=None,
                 snapchat=None,
                 biography=None):

        self.firstname = firstname
        self.email = email
        self.password = password
        self.birthdate = birthdate
        self.astro_sign_id = astro_sign_id
        self.city_id = city_id
        self.gender_id = gender_id
        self.fb = fb
        self.instagram = instagram
        self.snapchat = snapchat
        self.biography = biography
        self.search_astro_sign_id = search_astro_sign_id
        self.search_gender_id = search_gender_id
        self.search_city_id = search_city_id
        self.verif_email = verif_email
        self.verif_account = verif_account
        self.role_id = role_id

    def __repr__(self):
        return '<Utilisateur {}>'.format(self.firstname)
