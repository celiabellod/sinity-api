from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from ..database import Base

class Gender(Base):
    __tablename__ = 'genders'

    id = Column(
        Integer, 
        primary_key=True
    )
    
    name = Column(
        String(10), 
        nullable=False
    )
    
    def __init__(self, name):
        self.name = name

    def __repr__(self): 
        return '<Sexe {}>'.format(self.name)