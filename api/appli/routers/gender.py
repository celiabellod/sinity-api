from fastapi import APIRouter
from typing import List
from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from ..dependencies import oauth2_scheme
from ..database import get_db
from ..crud import gender as crud
from ..schemas import Gender as schemas

router = APIRouter(
    prefix="/genders",
    tags=["Genders"],
    responses={404: {"description": "Not found"}},
)

@router.get("", response_model=List[schemas.Gender])
def get_genders(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    genders = crud.get_genders(db, skip=skip, limit=limit)
    return genders

@router.get("/{gender_id}", response_model=schemas.Gender, dependencies=[Depends(oauth2_scheme)])
def get_gender(gender_id: int, db: Session = Depends(get_db)):
    db_gender = crud.get_gender(db, gender_id=gender_id)
    if db_gender is None:
        raise HTTPException(status_code=404, detail="Gender not found")
    return db_gender