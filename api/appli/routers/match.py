from fastapi import APIRouter
from typing import List
from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from ..crud import admin as crud_admin
from ..dependencies import *
from ..database import settings
from ..database import get_db
from ..crud import match as crud
from ..schemas import Match as schemas

router = APIRouter(
    prefix="/matches",
    tags=["Matches"],
    dependencies=[Depends(oauth2_scheme)],
    responses={404: {"description": "Not found"}},
)

@router.get("", response_model=List[schemas.MatchOut])
def get_matches(skip: int = 0, limit: int = 100, db: Session = Depends(get_db), token: str = Depends(oauth2_scheme)):
    payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM])
    admin = crud_admin.get_admin_by_email(email=payload.get("sub"), db=db)
    if admin is None:
        raise credentials_exception
    
    matches = crud.get_matches(db, skip=skip, limit=limit)
    if matches is None:
        raise HTTPException(status_code=404, detail="Matches not found")
    return matches

@router.get("/{match_id}", response_model=schemas.MatchOut)
def get_match(match_id: int, db: Session = Depends(get_db), token: str = Depends(oauth2_scheme)):
    payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM])
    admin = crud_admin.get_admin_by_email(email=payload.get("sub"), db=db)
    if admin is None:
        raise credentials_exception
    
    match = crud.get_match(db, match_id=match_id)
    if match is None:
        raise HTTPException(status_code=404, detail="Match not found")
    return match

@router.get("/{user_id}/{match_user_id}", response_model=List[schemas.MatchOut])
def get_user_match(user_id: int, match_user_id: int, db: Session = Depends(get_db), token: str = Depends(oauth2_scheme)):
    payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM])
    admin = crud_admin.get_admin_by_email(email=payload.get("sub"), db=db)
    if admin is None:
        raise credentials_exception
    
    match = crud.get_user_match(db, user_id=user_id, match_user_id=match_user_id)
    if match is None:
        raise HTTPException(status_code=404, detail="Match not found")
    return match

@router.get("/{user_id}", response_model=List[schemas.MatchOut])
def get_matches_by_user(user_id: int, db: Session = Depends(get_db), token: str = Depends(oauth2_scheme)):
    payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM])
    admin = crud_admin.get_admin_by_email(email=payload.get("sub"), db=db)
    if admin is None:
        raise credentials_exception
    
    matches = crud.get_matches_by_user(db, user_id=user_id)
    if matches is None:
        raise HTTPException(status_code=404, detail="Matches not found")
    return matches

@router.post("", response_model=schemas.MatchIn)
def create_match(match: schemas.MatchIn, db: Session = Depends(get_db), token: str = Depends(oauth2_scheme)):
    payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM])
    admin = crud_admin.get_admin_by_email(email=payload.get("sub"), db=db)
    if admin is None:
        raise credentials_exception
    
    #verif match already exists
    return crud.create_match(db=db, match=match)

@router.delete("/{match_id}",  responses={
        200: { "description": "Match delete with success." }
    }
)
def delete_match(match_id: int, db: Session = Depends(get_db), token: str = Depends(oauth2_scheme)):
    payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM])
    admin = crud_admin.get_admin_by_email(email=payload.get("sub"), db=db)
    if admin is None:
        raise credentials_exception
    
    existing_match = crud.get_match(db, match_id=match_id)
    if existing_match is None:
        raise HTTPException(status_code=404, detail="match not found")
    return crud.delete_match(db=db, match_id=match_id)