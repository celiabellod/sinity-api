from fastapi import APIRouter
from typing import List
from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from datetime import date
from ..schemas import AstroSign as schemas
from ..database import get_db
from ..crud import astro_sign as crud
from ..dependencies import oauth2_scheme

router = APIRouter(
    prefix="/astroSigns",
    tags=["AstroSigns"],
    responses={404: {"description": "Not found"}},
)

@router.get("", response_model=List[schemas.AstroSign])
def get_astro_signs(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    astro_signs = crud.get_astro_signs(db, skip=skip, limit=limit)
    return astro_signs

@router.get("/date")
def get_astro_sign_by_date(date: date, db: Session = Depends(get_db)):
    astro_sign = crud.get_astro_sign_by_date(db, date=date)
    if astro_sign is None:
        raise HTTPException(status_code=404, detail="Astro sign not found")
    return astro_sign
    
@router.get("/{astro_sign_id}", response_model=schemas.AstroSign)
def get_astro_sign(astro_sign_id: int, db: Session = Depends(get_db)):
    astro_sign = crud.get_astro_sign(db, astro_sign_id=astro_sign_id)
    if astro_sign is None:
        raise HTTPException(status_code=404, detail="Astro sign not found")
    return astro_sign