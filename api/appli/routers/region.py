from fastapi import APIRouter
from typing import List
from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from ..database import get_db
from ..crud import region as crud
from ..schemas import Region as schemas
from ..dependencies import oauth2_scheme

router = APIRouter(
    prefix="/regions",
    tags=["Regions"],
    dependencies=[Depends(oauth2_scheme)],
    responses={404: {"description": "Not found"}},
)

@router.get("/", response_model=List[schemas.Region])
def get_regions(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    regions = crud.get_regions(db, skip=skip, limit=limit)
    return regions

@router.get("/{region_id}", response_model=schemas.Region)
def get_region(region_id: int, db: Session = Depends(get_db)):
    db_region = crud.get_region(db, region_id=region_id)
    if db_region is None:
        raise HTTPException(status_code=404, detail="Region not found")
    return db_region