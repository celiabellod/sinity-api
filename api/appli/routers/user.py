from fastapi import APIRouter
from typing import List
from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from passlib.context import CryptContext
from fastapi import File, UploadFile
from ..schemas import User as schemas
from ..schemas import Picture as schemas_picture
from ..database import get_db
from ..crud import user as crud
from ..crud import admin as crud_admin
from ..dependencies import *
from ..database import settings
from ..schemas.Token import Token, TokenData

router = APIRouter(
    prefix="/users",
    tags=["Users"],
    responses={404: {"description": "Not found"}},
)

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

@router.get("", response_model=List[schemas.UserOut])
def get_users(role_id: int = 1, gender_id: int = 0, astro_sign_id: int = 0, city_id: int = 0, skip: int = 0, limit: int = 100, token: str = Depends(oauth2_scheme), db: Session = Depends(get_db)):
    
    try:
        payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM]) 
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = TokenData(username=username)
    except JWTError as e:
        HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=e,
            headers={"WWW-Authenticate": "Bearer"},
        )
    
    users = crud.get_users(db, role_id, gender_id, astro_sign_id, city_id, skip=skip, limit=limit)
    return users

@router.get("/{user_id}", response_model=schemas.UserOut)
def get_user(user_id: int, db: Session = Depends(get_db), token: str = Depends(oauth2_scheme)):
   
    payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM])
    admin = crud_admin.get_admin_by_email(email=payload.get("sub"), db=db)
    if admin is None:
        raise credentials_exception

    db_user = crud.get_user(db, user_id=user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user

@router.post("", response_model=schemas.UserIn)
def create_user(user: schemas.UserIn, db: Session = Depends(get_db)):
    db_user = crud.get_user_by_email(db, email=user.email)
    if db_user:
        raise HTTPException(status_code=400, detail="User already registered")
    
    user.password = hash_password(user.password)
    return crud.create_user(db=db, user=user)

@router.put("/{user_id}", response_model=schemas.UserOut)
def update_user(user_id: int, user: schemas.UserIn, db: Session = Depends(get_db), token: str = Depends(oauth2_scheme)):
    
    payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM])
    admin = crud_admin.get_admin_by_email(email=payload.get("sub"), db=db)
    if admin is None:
        raise credentials_exception
    
    existing_user = crud.get_user(db, user_id=user_id)
    if existing_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return crud.update_user(db=db, user_id=user_id, user=user)

@router.delete("/{user_id}",  responses={
        200: { "description": "User delete with success." }
    },
    dependencies=[Depends(oauth2_scheme)],
)
def delete_user(user_id: int, db: Session = Depends(get_db), token: str = Depends(oauth2_scheme)):
    
    payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM])
    admin = crud_admin.get_admin_by_email(email=payload.get("sub"), db=db)
    if admin is None:
        raise credentials_exception
    
    existing_user = crud.get_user(db, user_id=user_id)
    if existing_user is False:
        raise HTTPException(status_code=404, detail="User not found")
    
    delete_user = crud.delete_user(db=db, user_id=user_id)
    if delete_user is False:
        raise HTTPException(status_code=404, detail="User can't be deleted")
    
    return "User delete with success."

def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)

def hash_password(password):
    return pwd_context.hash(password)


@router.post("/{user_id}/pictures")
def upload_picture(user_id: int, file: UploadFile = File(...), db: Session = Depends(get_db), token: str = Depends(oauth2_scheme)):
    
    payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM])
    admin = crud_admin.get_admin_by_email(email=payload.get("sub"), db=db)
    if admin is None:
        raise credentials_exception
    
    upload_picture = crud.upload_picture(db=db, user_id=user_id, file=file)
    if upload_picture is None: 
        raise HTTPException(status_code=404, detail="Picture upload didn't work")
    return upload_picture

@router.get("/{user_id}/pictures",)
def get_pictures(user_id: int, db: Session = Depends(get_db), token: str = Depends(oauth2_scheme)):
   
    try:
        payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM]) 
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = TokenData(username=username)
    except JWTError as e:
        HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=e,
            headers={"WWW-Authenticate": "Bearer"},
        )
        
    user = crud.get_user_by_email(db, email=token_data.username)
    if user is None:
        raise credentials_exception
    
    pictures = crud.get_pictures_by_user(db=db, user_id=user_id)
    if pictures is None: 
        raise HTTPException(status_code=404, detail="Picture not found")
    return pictures
# response_model=List[schemas_picture.Picture]

@router.get("/gender/{gender_id}", response_model=List[schemas.UserOut])
def get_users_by_gender(gender_id: int, db: Session = Depends(get_db), token: str = Depends(oauth2_scheme)):
    
    payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM])
    admin = crud_admin.get_admin_by_email(email=payload.get("sub"), db=db)
    if admin is None:
        raise credentials_exception
    
    users = crud.get_users_by_gender(db,gender_id=gender_id)
    return users