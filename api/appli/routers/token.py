from fastapi import status
from fastapi.security import OAuth2PasswordRequestForm
from datetime import datetime, timedelta
from typing import Optional
from ..schemas.Token import Token, TokenData
from ..database import settings
from .user import *
from ..crud.admin import get_admin_by_email

router = APIRouter(
    tags=["token"],
)

@router.post("/token", response_model=Token)
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)):
    user = authenticate_user(username=form_data.username, password=form_data.password, db=db)
    if user is None:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect email or password",
            headers={"WWW-Authenticate": "Bearer"},
        )

    access_token = create_token(user.email)
    refresh_acces_token = refresh_token(user.email)
    return {"user_data": user, "access_token": access_token, "token_type": "bearer", "refresh_token": refresh_acces_token}


def authenticate_user(username: str, password: str, db: Session = Depends(get_db)):
    user = crud.get_user_by_email(db=db, email=username)
    
    if not user:
        admin = get_admin_by_email(db=db, email=username)
        if not admin:
            return None
        user = admin
        
    if not verify_password(password, user.password):
        return None

    return user

def create_access_token(data: dict, expires_delta: Optional[timedelta] = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, settings.SECRET_KEY, algorithm=settings.ALGORITHM)
    return encoded_jwt

def create_token(email):
    access_token_expires = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(data={'sub': email}, expires_delta=access_token_expires)
    return access_token

def refresh_token(email):
    access_token_expires = timedelta(minutes=settings.REFRESH_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(data={'sub': email}, expires_delta=access_token_expires)
    return access_token

# @router.post("/refresh-token", response_model=str)
# def refresh_token(access_token: Token, db: Session = Depends(get_db)):
#     refresh_token = access_token.refresh_token
#     token_user = get_current_user(token = refresh_token,db=db)

#     login_token = create_token(token_user.email)

#     return login_token

async def get_current_user(token: str = Depends(oauth2_scheme), db: Session = Depends(get_db)):
    try:
        payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = TokenData(username=username)
    except JWTError:
        raise credentials_exception
    user = crud.get_user_by_email(db, email=token_data.username)
    if user is None:
        raise credentials_exception
    return user

@router.get("/users/me/", response_model=schemas.UserOut)
async def read_users_me(current_user: schemas.UserOut = Depends(get_current_user)):
    return current_user

