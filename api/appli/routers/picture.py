from fastapi import APIRouter
from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from ..schemas import Picture as schemas
from ..database import get_db
from ..crud import picture as crud
from fastapi import Depends, HTTPException
from ..crud import user as user_crud
from ..dependencies import *
from ..database import settings

router = APIRouter(
    prefix="/picture",
    tags=["Picture"],
    dependencies=[Depends(oauth2_scheme)],
    responses={404: {"description": "Not found"}},
)

@router.get("/{picture_id}", response_model=schemas.Picture)
def get_picture(picture_id: int, db: Session = Depends(get_db)):
    picture = crud.get_picture(db, picture_id=picture_id)
    if picture is None:
        raise HTTPException(status_code=404, detail="Picture not found")
    return picture


@router.delete("/{picture_id}",  responses={
        200: { "description": "Picture delete with success." }
    }
)
def delete_picture(picture_id: int, db: Session = Depends(get_db), token: str = Depends(oauth2_scheme)):
    payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM])
    user = user_crud.get_user_by_email(email=payload.get("sub"), db=db)

    if(user.role.name == 'USER'):
        raise credentials_exception
    
    existing_picture = crud.get_picture(db, picture_id=picture_id)
    if existing_picture is False:
        raise HTTPException(status_code=404, detail="Picture not found")
    crud.delete_picture(db=db, picture_id=picture_id)
    return "Picture delete with success."