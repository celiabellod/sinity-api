from sqlalchemy.orm import Session
from ..models import Match as models
from ..schemas import Match as schemas

LIMIT = 100
SKIP = 0

def get_matches(db: Session, skip: int = SKIP, limit: int = LIMIT):
    return db.query(models.Match).offset(skip).limit(limit).all()

def get_match(db: Session, match_id: int):
     return db.query(models.Match).filter(models.Match.id == match_id).first()

def get_user_match(db: Session, user_id: int, match_user_id: int):  
    return db.query(models.Match).filter(models.Match.user_id == user_id).filter(models.Match.match_user_id == match_user_id).first()

def get_matches_by_user(db: Session, user_id: int):
    return db.query(models.Match).filter(models.Match.user_id == user_id).all()

def create_match(db: Session, match: schemas.MatchIn):
    db_match = models.Match(
        user_id=match.user_id,
        match_user_id=match.match_user_id
    )
    db.add(db_match)
    db.commit()
    db.refresh(db_match)
    return db_match

def delete_match(db: Session, match_id: int):
    try:
        db_match = db.query(models.Match).filter(models.Match.id == match_id).first()
        db.delete(db_match)
        db.commit()
        return True
    except:
        return False