from sqlalchemy.orm import Session
from ..models.Role import Role as model

LIMIT = 100
SKIP = 0

def get_roles(db: Session, skip: int = SKIP, limit: int = LIMIT):
    return db.query(model).offset(skip).limit(limit).all()

def get_role(db: Session, role_id: int):
    return db.query(model).filter(model.id == role_id).first()