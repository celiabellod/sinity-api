from sqlalchemy.orm import Session
from ..models import Picture as models
import os
from fastapi.responses import FileResponse

def get_picture(db: Session, picture_id: int):
    picture = db.query(models.Picture).filter(models.Picture.id == picture_id).first()
    filepath = './upload/' + picture.name
    if os.path.exists(filepath):
         return FileResponse(filepath, media_type="image/png", filename=picture.name)
     
    return {"error" : "File not found!"}

def delete_picture(db: Session, picture_id: int):
    try:
        db_picture = db.query(models.Picture).filter(
            models.Picture.id == picture_id).first()
        db.delete(db_picture)
        db.commit()
        return True
    except:
        return False