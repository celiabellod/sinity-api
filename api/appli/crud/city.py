from sqlalchemy.orm import Session
from ..models.City import City as model

LIMIT = 100
SKIP = 0

def get_cities(db: Session, skip: int = SKIP, limit: int = LIMIT):
    return db.query(model).offset(skip).limit(limit).all()

def get_city(db: Session, city_id: int):
    return db.query(model).filter(model.id == city_id).first()