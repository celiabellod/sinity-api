from sqlalchemy.orm import Session
from datetime import date
from ..models.AstroSign import AstroSign as model

YEAR = 1970
LIMIT = 100
SKIP = 0

def get_astro_signs(db: Session, skip: int = SKIP, limit: int = LIMIT):
    return db.query(model).offset(skip).limit(limit).all()

def get_astro_sign(db: Session, astro_sign_id: int):
    return db.query(model).filter(model.id == astro_sign_id).first()

def get_astro_sign_by_date(db: Session, date: date):
    date = date.replace(year=YEAR) 
    return db.query(model).filter(model.dateFrom <= date).filter(model.dateTo >= date).first()