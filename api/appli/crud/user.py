from sqlalchemy.orm import Session
from sqlalchemy import update
from sqlalchemy.sql.expression import false
from ..models import User as models
from ..models import Picture as models_picture
from ..schemas import User as schemas
from fastapi import File, UploadFile
import uuid
import os
from fastapi.responses import FileResponse

LIMIT = 100
SKIP = 0
DEFAULT_ROLE_ID = 1
DEFAULT_GENDER_ID = 0
DEFAULT_ASTRO_SIGN_ID = 0
DEFAULT_CITY_ID = 0

def get_users(db: Session, 
              role_id: int = DEFAULT_ROLE_ID, 
              gender_id: int = DEFAULT_GENDER_ID, 
              astro_sign_id: int = DEFAULT_ASTRO_SIGN_ID, 
              city_id: int = DEFAULT_CITY_ID, 
              skip: int = SKIP, 
              limit: int = LIMIT
            ):
    query = db.query(models.User).filter(models.User.role_id == role_id)

    if gender_id is not 0:
        query = query.filter(models.User.gender_id == gender_id)

    if astro_sign_id is not 0:
        query = query.filter(models.User.astro_sign_id == astro_sign_id)

    if city_id is not 0:
        query = query.filter(models.User.city_id == city_id)

    query = query.offset(skip).limit(limit).all()
    return query


def get_user(db: Session, user_id: int):
    return db.query(models.User).filter(models.User.id == user_id).first()


def get_user_by_email(db: Session, email: str):
    return db.query(models.User).filter(models.User.email == email).first()


def get_users_by_gender(db: Session, gender_id: int):
    return db.query(models.User).filter(models.User.gender_id == gender_id).all()


def create_user(db: Session, user: schemas.UserIn):

    db_user = models.User(
        firstname=user.firstname,
        email=user.email,
        password=user.password,
        birthdate=user.birthdate,
        astro_sign_id=user.astro_sign_id,
        city_id=user.city_id,
        gender_id=user.gender_id,
        search_astro_sign_id=user.search_astro_sign_id,
        search_gender_id=user.search_gender_id,
        search_city_id=user.search_city_id,
        fb=user.fb if user.fb else None,
        snapchat=user.snapchat if user.snapchat else None,
        instagram=user.instagram if user.instagram else None,
        biography=user.biography if user.biography else None,
        verif_email=False,
        verif_account=False,
        role_id=user.role_id if user.role_id else DEFAULT_ROLE_ID
    )
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def update_user(db: Session, user_id: int, user: schemas.UserIn):
    db_user = db.query(models.User).filter(models.User.id == user_id).first()

    setattr(db_user, "firstname", user.firstname)
    setattr(db_user, "email", user.email)
    setattr(db_user, "password", user.password)
    setattr(db_user, "birthdate", user.birthdate)
    setattr(db_user, "astro_sign_id", user.astro_sign_id)
    setattr(db_user, "city_id", user.city_id)
    setattr(db_user, "gender_id", user.gender_id)
    setattr(db_user, "search_astro_sign_id", user.search_astro_sign_id)
    setattr(db_user, "search_gender_id", user.search_gender_id)
    setattr(db_user, "search_city_id", user.search_city_id)
    setattr(db_user, "fb", user.fb)
    setattr(db_user, "snapchat", user.snapchat)
    setattr(db_user, "instagram", user.instagram)
    setattr(db_user, "fb", user.fb)
    setattr(db_user, "biography", user.biography)
    setattr(db_user, "role_id", user.role_id)

    try:
        setattr(db_user, "verif_email", user.verifEmail)
    except:
        pass
    try:
        setattr(db_user, "verif_account", user.verifAccount)
    except:
        pass

    db.commit()
    db.flush(db_user)
    return db_user


def delete_user(db: Session, user_id: int):
    try:
        db_user = db.query(models.User).filter(
            models.User.id == user_id).first()
        db.delete(db_user)
        db.commit()
    except:
        return False


def upload_picture(db: Session, user_id: int, file: UploadFile = File(...)):
    filename = os.path.basename(f'{uuid.uuid4()}-{file.filename}')
    if open('./upload/' + filename, 'wb').write(file.file.read()):
        picture = models_picture.Picture(
            name=filename,
            user_id=user_id
        )
        db.add(picture)
        db.commit()
        db.refresh(picture)
        return picture


def get_pictures_by_user(db: Session, user_id: int):
    # later send all image
    picture = db.query(models_picture.Picture).filter(
        models_picture.Picture.user_id == user_id).first()
    if(picture is not None):
        filepath = './upload/' + picture.name
        if os.path.exists(filepath):
            return FileResponse(filepath, media_type="image/png", filename=picture.name)

    return None
