from sqlalchemy.orm import Session
from ..models.Region import Region as model

LIMIT = 100
SKIP = 0

def get_regions(db: Session, skip: int = SKIP, limit: int = LIMIT):
    return db.query(model).offset(skip).limit(limit).all()

def get_region(db: Session, region_id: int):
    return db.query(model).filter(model.id == region_id).first()
