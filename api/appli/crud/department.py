from sqlalchemy.orm import Session
from ..models.Department import Department as model

LIMIT = 100
SKIP = 0

def get_departments(db: Session, skip: int = SKIP, limit: int = LIMIT):
    return db.query(model).offset(skip).limit(limit).all()

def get_department(db: Session, department_id: int):
    return db.query(model).filter(model.id == department_id).first()