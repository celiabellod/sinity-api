from sqlalchemy.orm import Session
from ..models import Admin as models
from ..schemas import Admin as schemas

def get_admin_by_email(db: Session, email: str):
    return db.query(models.Admin).filter(models.Admin.email == email).first()

def create_admin(db: Session, admin: schemas.AdminIn):
    
    db_admin = models.Admin(
        firstname=admin.firstname,
        lastname=admin.lastname,
        email=admin.email,
        password=admin.password,
    )
    db.add(db_admin)
    db.commit()
    db.refresh(db_admin)
    return db_admin

def delete_admin(db: Session, admin_id: int):
    try:
        db_admin = db.query(models.Admin).filter(models.Admin.id == admin_id).first()
        db.delete(db_admin)
        db.commit()
    except:
        return False