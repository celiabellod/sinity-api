from sqlalchemy.orm import Session
from ..models.Gender import Gender as model

LIMIT = 100
SKIP = 0

def get_genders(db: Session, skip: int = SKIP, limit: int = LIMIT):
    return db.query(model).offset(skip).limit(limit).all()

def get_gender(db: Session, gender_id: int):
    return db.query(model).filter(model.id == gender_id).first()