from appli.database import engine, Base
from sqlalchemy.sql import text
from appli.models import Region, City, Department, AstroSign, Gender, User, Match, Picture, Role

with engine.connect() as con:

    if engine.dialect.has_table(con, 'regions'):
        file = open("sql-database/tables/regions.sql")
        query = text(file.read())
        con.execute(query)

    if engine.dialect.has_table(con, 'departments'):
        file = open("sql-database/tables/departments.sql")
        query = text(file.read())
        con.execute(query)

    if engine.dialect.has_table(con, 'cities'):
        file = open("sql-database/tables/cities.sql")
        query = text(file.read())
        con.execute(query)

    if engine.dialect.has_table(con, 'genders'):
        file = open("sql-database/tables/genders.sql")
        query = text(file.read())
        con.execute(query)

    if engine.dialect.has_table(con, 'roles'):
        file = open("sql-database/tables/roles.sql")
        query = text(file.read())
        con.execute(query)

    if engine.dialect.has_table(con, 'astro_signs'):
        file = open("sql-database/tables/astroSigns.sql")
        query = text(file.read())
        con.execute(query)
