from pydantic import BaseSettings
import os
class Settings(BaseSettings):
    DATABASE_URL = os.environ['DATABASE_URL']
    SECRET_KEY =  os.environ['SECRET_KEY']
    ALGORITHM = "HS256"
    ACCESS_TOKEN_EXPIRE_MINUTES = 30
    REFRESH_TOKEN_EXPIRE_MINUTES = 60 * 24 * 30

    class Config:
        env_file = ".env"