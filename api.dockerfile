FROM python:3.9-buster
WORKDIR /usr/src/api
COPY ./api /usr/src/api
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
EXPOSE 8000